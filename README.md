# What is this?
It's a template for Python 3.6.x projects. It aims to provide a 
reasonable structure and opinionated defaults for code quality. The ultimate 
goal is to make your code predictable and stable even as the project grows.
It covers the following types of code quality checks out of the box:
 * type checking (via mypy)
 * unit tests
 * code coverage (via coverage)
 * code complexity analysis (via radon)
 * import ordering (via isort)
 * general linting rules (via flake8)

The code inside `src` and `tests` is only there for demonstration purposes.

## How should I use this?
- Activate a virtual Python 3.6 virtualenv
- Install testing requirements: `pip install -r requirements/test.txt`
- Run `python run_tests.py` and fix errors (Keep running the script as you fix errors 
-- the default code in this project is contrived to trigger a number types of 
failures, so you get an idea of the types of issues it addresses)
- **Replace the readme, boilerplate code, and tests with your own stuff!**
- If you cloned this repo, delete the `.git` directory (`rm -rf ./.git`) and 
start your own repo (`git init`)!
- Tweak as you go. (remember to keep increasing the coverage minimum 
threshold as you go -- try to get to 100%!) 


### Tips
- If isort complains about imports, it's generally reliable to run `isort -y` to 
automatically restructure them.
- A tool like YAPF (https://github.com/google/yapf) can also be nice to use
as a file-watcher if you want formatting to be handled automatically.
- `typing_extensions` may be worth considering if you want to define recursive
types
- `f-strings` and `NamedTuples` offer nice syntax improvements in Python 3.6. 
Use them!