from src.hello import get_greeting
from unittest import TestCase


class HelloTests(TestCase):
    def test_greeting_returns_hello_with_name(self):
        self.assertEqual(f"Hello, Bob!", get_greeting("Bob"))
