#!/usr/bin/env python3

import os
import subprocess
import sys
import time


def do_call(args) -> None:
    oneline = ' '.join([f'"{x}"' for x in args])

    print(f'[{os.getcwd()}]> {oneline}]')
    try:
        subprocess.check_output(args, env=os.environ)
    except subprocess.CalledProcessError as error:
        print(error)
        print(error.output.decode())
        sys.exit(1)


def run_tests() -> None:
    print('Running tests with coverage')
    do_call(['coverage', 'run', '-m', 'unittest', 'discover', 'tests'])

    print('checking coverage')

    # low enough so we're able to pass at start. Aim to get this to 100
    do_call(['coverage', 'report', '--fail-under', '80'])

    do_call(['coverage', 'erase'])


def run_mypy(dir_name: str) -> None:
    print(f'Running mypy type checking of {dir_name}')
    do_call(['mypy', dir_name,
             '--check-untyped-defs',
             '--strict-optional',
             '--ignore-missing-imports'])


def run_flake8() -> None:
    print('Running flake')
    do_call(['flake8', '.'])


if __name__ == "__main__":
    start_time = time.time()

    run_mypy('src')
    run_mypy('tests')

    run_flake8()

    run_tests()

    print(f'TESTS PASSED in {time.time() - start_time} seconds! '
          f'YOU ARE A GOOD PERSON! :)')
