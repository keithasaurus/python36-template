from typing import Any  # Unused import; only here to demonstrate isort checks
from hello import get_greeting


if __name__ == '__main__':
    name = input("What is your name? \n")
    print(get_greeting(name))
