def get_greeting(name: str) -> int:  # the `int` return type is invalid

    # this code is contrived to be exceed code complexity settings
    if name == "Bob":
        greeting = "Not Hello"
    elif name == "Alice":
        greeting = "Good day"
    elif name == "Carl":
        greeting = "What's up"
    elif name == "Daniel":
        greeting = "Yo"
    elif name == "Evan":
        greeting = "Hey"
    elif name == "Frank":
        greeting = "Bonjour"
    elif name == "Grace":
        greeting = "Top of the morning to you"
    else:
        greeting = "Hello"

    return f"{greeting}, {name}!"
